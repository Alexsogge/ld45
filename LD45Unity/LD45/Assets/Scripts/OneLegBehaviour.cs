﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneLegBehaviour : StateMachineBehaviour
{
    public GameObject player;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsName("RobotArmature|Jump"))
        {
            player.GetComponent<PlayerControl>().Jump();
        }
        if (stateInfo.IsName("RobotArmature|JumpLand"))
        {
            player.GetComponent<PlayerControl>().Landed();
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //    // Implement code that processes and affects root motion
        //Debug.Log(stateInfo);
        //AnimatorClipInfo[] animationClip = animator.GetCurrentAnimatorClipInfo(0);
        //int currentFrame = (int)(animationClip[0].weight * (animationClip[0].clip.length * animationClip[0].clip.frameRate));
        //Debug.Log(currentFrame);

    }

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
