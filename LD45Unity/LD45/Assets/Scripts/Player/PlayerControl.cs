﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    public GameObject cameraDrone;
    public GameObject infoPanel;
    public float rollingspeed = 300;
    public float crawlSpeed = 1000;
    public float maxCrawlSpeed = 10;
    public float maxCrawlSpeedWithArm = 15;
    public float walkSpeedOneLeg = 1500;
    public float walkSpeedTwoLegs = 2500;
    public float maxWalklSpeedWithOneLeg = 16;
    public float maxWalklSpeedWithTwoLegs = 20;
    public float oneLegJumpForce = 8;
    public float twoLegJumpForce = 13;
    private bool jumping = false;

    public int bodyState = 0;

    private Rigidbody rigidbody;
    private GameObject model;
    private Collider tmpCollider;
    
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();
        model = transform.Find("RobotHead").gameObject;
        InitState(bodyState);
    }

    // Update is called once per frame
    void Update()
    {
        UserMouseClicks();
    }

    private void FixedUpdate()
    {
        float inputAxisX = Input.GetAxis("Vertical");
        float inputAxisZ = Input.GetAxis("Horizontal");

        if (bodyState == 0)
        {
            float movementZ = inputAxisX * Time.deltaTime * rollingspeed;
            float movementX = inputAxisZ * Time.deltaTime * rollingspeed;

            Vector3 moveStep = new Vector3(movementX, 0, movementZ);
            moveStep = Quaternion.AngleAxis(cameraDrone.GetComponent<CameraDrone>().angle * -Mathf.Rad2Deg, Vector3.up) * moveStep;
            //Debug.Log(cameraDrone.GetComponent<CameraDrone>().angle+ " -> " +Quaternion.AngleAxis(cameraDrone.GetComponent<CameraDrone>().angle, Vector3.up));
            rigidbody.AddTorque(moveStep);
            
        }
        else if (bodyState == 1)
        {
            float movementZ = inputAxisZ * Time.deltaTime * 100;
            float movementX = inputAxisX * Time.deltaTime * crawlSpeed;

            model.GetComponent<Animator>().SetFloat("Crawlspeed", Mathf.Abs(inputAxisX) + Mathf.Abs(inputAxisZ));
            //Debug.Log("Speed: " + rigidbody.velocity.sqrMagnitude);
            if (rigidbody.velocity.sqrMagnitude < maxCrawlSpeed)
            {
                rigidbody.AddForce(transform.rotation * Vector3.forward * movementX, ForceMode.Acceleration);
            }
            else
            {
                //Debug.Log("Maxspeed: " + rigidbody.velocity.sqrMagnitude);
            }
            transform.Rotate(Vector3.up * movementZ, Space.World);
            //transform.Translate(Vector3.forward * movementX);
        }
        else if (bodyState == 2)
        {
            float movementZ = inputAxisZ * Time.deltaTime * 100;
            float movementX = inputAxisX * Time.deltaTime * crawlSpeed * 1.2f;

            model.GetComponent<Animator>().SetFloat("Crawlspeed", Mathf.Abs(inputAxisX) + Mathf.Abs(inputAxisZ / 2));
            //Debug.Log("Speed: " + rigidbody.velocity.sqrMagnitude);
            if (rigidbody.velocity.sqrMagnitude < maxCrawlSpeedWithArm)
            {
                rigidbody.AddForce(transform.rotation * Vector3.forward * movementX, ForceMode.Acceleration);
            }
            transform.Rotate(Vector3.up * movementZ, Space.World);
        }
        else if (bodyState == 3)
        {
            float movementZ = inputAxisZ * Time.deltaTime * 100;
            float movementX = inputAxisX * Time.deltaTime * walkSpeedOneLeg;
       
            model.GetComponent<Animator>().SetFloat("WalkSpeed", Mathf.Abs(inputAxisX) + Mathf.Abs(inputAxisZ));

            if (rigidbody.velocity.sqrMagnitude < maxWalklSpeedWithOneLeg)
            {
                rigidbody.AddForce(transform.rotation * Vector3.forward * movementX, ForceMode.Acceleration);
                //rigidbody.AddForce(Vector3.up * 2, ForceMode.Impulse);
            }

            if (Input.GetButtonDown("Jump") && !jumping)
            {
                jumping = true;
                model.GetComponent<Animator>().SetTrigger("Jump");
            }

            transform.Rotate(Vector3.up * movementZ, Space.World);
        }
        else if (bodyState == 4)
        {
            float movementZ = inputAxisZ * Time.deltaTime * 100;
            float movementX = inputAxisX * Time.deltaTime * walkSpeedTwoLegs;

            model.GetComponent<Animator>().SetFloat("WalkSpeed", Mathf.Abs(inputAxisX) + Mathf.Abs(inputAxisZ));

            if (rigidbody.velocity.sqrMagnitude < maxWalklSpeedWithTwoLegs)
            {
                rigidbody.AddForce(transform.rotation * Vector3.forward * movementX, ForceMode.Acceleration);
                //rigidbody.AddForce(Vector3.up * 2, ForceMode.Impulse);
            }

            if (Input.GetButtonDown("Jump") && !jumping)
            {
                jumping = true;
                model.GetComponent<Animator>().SetTrigger("Jump");
            }

            transform.Rotate(Vector3.up * movementZ, Space.World);
        }
        else if (bodyState == 5)
        {
            float movementZ = inputAxisZ * Time.deltaTime * 100;
            float movementX = inputAxisX * Time.deltaTime * walkSpeedTwoLegs;

            model.GetComponent<Animator>().SetFloat("WalkSpeed", Mathf.Abs(inputAxisX) + Mathf.Abs(inputAxisZ));

            if (rigidbody.velocity.sqrMagnitude < maxWalklSpeedWithTwoLegs)
            {
                rigidbody.AddForce(transform.rotation * Vector3.forward * movementX, ForceMode.Acceleration);
                //rigidbody.AddForce(Vector3.up * 2, ForceMode.Impulse);
            }

            if (Input.GetButtonDown("Jump") && !jumping)
            {
                jumping = true;
                model.GetComponent<Animator>().SetTrigger("Jump");
            }

            transform.Rotate(Vector3.up * movementZ, Space.World);
        }
    }

    private void UserMouseClicks()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100))
        {
            if (hit.transform.tag == "Barrier")
                hit.transform.parent.gameObject.GetComponent<Barrier>().Highlight();
            if (hit.transform.tag == "Crane")
                hit.transform.parent.gameObject.GetComponent<Crane>().Highlight();
            //Debug.Log(hit.transform.gameObject.name);
            if (bodyState >= 2)
            {
                if (hit.transform.name == "Button")
                {
                    //Debug.Log(Vector3.Magnitude(hit.transform.position - transform.position));
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (Vector3.Magnitude(hit.transform.position - transform.position) < 10)
                        {
                            if(hit.transform.tag == "Barrier")
                                hit.transform.parent.gameObject.GetComponent<Barrier>().ToggleState();
                            else if (hit.transform.tag == "Crane")
                                hit.transform.parent.gameObject.GetComponent<Crane>().ToggleState();
                        }
                    }
                }
            }
            if (bodyState == 5 && hit.transform.tag == "Climbable")
            {
                if(Input.GetMouseButtonDown(0) && Vector3.Magnitude(hit.transform.position - transform.position) < 7)
                    Climb();
            }
        }
    }

    private void InitState(int state)
    {
        this.bodyState = state;
        if (model != null)
            model.SetActive(false);
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;

        infoPanel.transform.Find("Text" + state).gameObject.GetComponent<Text>().enabled = true;

        if (state == 0)
        {
            model = transform.Find("RobotHead").gameObject;
            model.SetActive(true);
            rigidbody.constraints = RigidbodyConstraints.None;
        }
        else if (state == 1)
        {
            model = transform.Find("RobotWithBody").gameObject;
            model.SetActive(true);
            rigidbody.constraints = RigidbodyConstraints.FreezeRotationZ;
            transform.rotation = Quaternion.identity;
        }
        else if (state == 2)
        {
            model = transform.Find("RobotWithArmR").gameObject;
            model.SetActive(true);
            rigidbody.constraints = RigidbodyConstraints.FreezeRotationZ;
            transform.rotation = Quaternion.identity;
        }
        else if (state == 3)
        {
            model = transform.Find("RobotWithLegR").gameObject;
            model.SetActive(true);
            model.GetComponent<Animator>().GetBehaviour<OneLegBehaviour>().player = gameObject;
            rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            transform.rotation = Quaternion.identity;
            transform.Translate(0, 8, 0);
        }
        else if (state == 4)
        {
            model = transform.Find("Robot2Leg").gameObject;
            model.SetActive(true);
            model.GetComponent<Animator>().GetBehaviour<OneLegBehaviour>().player = gameObject;
            rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            transform.rotation = Quaternion.identity;
            transform.Translate(0, 8, 0);
        }
        else if (state == 5)
        {
            model = transform.Find("RobotComplete").gameObject;
            model.SetActive(true);
            model.GetComponent<Animator>().GetBehaviour<OneLegBehaviour>().player = gameObject;
            rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            transform.rotation = Quaternion.identity;
            transform.Translate(0, 8, 0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.name);
        if (other.name == "Terrain" && bodyState >= 3 && jumping)
        {
            model.GetComponent<Animator>().SetTrigger("Landed");
            Landed();
        }
    }


    public void DockBody()
    {
        //Debug.Log("Dock body");
        InitState(bodyState + 1);
    }

    public void Jump()
    {
        float jumpforce = oneLegJumpForce;
        if (Input.GetButton("Sprint") && bodyState >= 4)
        {
            jumpforce *= 1.5f;
            rigidbody.AddForce(transform.rotation * Vector3.forward * jumpforce * 0.8f, ForceMode.Impulse);
        }
        rigidbody.AddForce(Vector3.up * jumpforce, ForceMode.Impulse);
    }
    public void Landed()
    {
        jumping = false;
    }

    private void Climb()
    {

        // Debug.Log("Climb");
        model.GetComponent<Animator>().SetTrigger("Climb");
        gameObject.GetComponent<Animator>().SetTrigger("Climb");
    }

    public void ClimbEvent(int step)
    {
        // Quick and dirty
        if (step == 0)
        {
            rigidbody.AddForce(Vector3.up * 14, ForceMode.Impulse);
        }
        if (step == 1)
        {
            //Debug.Log("Disable collider");
            tmpCollider = model.GetComponent<Collider>();
            tmpCollider.enabled = false;
        }
        if (step == 2)
        {
            rigidbody.AddForce(transform.rotation * Vector3.forward * 10, ForceMode.Impulse);
        }
        if (step == 3)
        {
            //Debug.Log("Enable collider");
            tmpCollider.enabled = true;
        }
    }
}
