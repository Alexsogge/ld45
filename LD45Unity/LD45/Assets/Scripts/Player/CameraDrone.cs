﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDrone : MonoBehaviour
{
    public GameObject focusObject;
    public Vector3 offset = new Vector3(10, 7, 0);
    public float lookSpeed = 1;
    public float moveSpeed = 5;
    public int maxDistance = 15;
    public int minDistance = 2;
    public int maxHeight = 15;
    public int minHeight = 1;

    private Vector2 rotation = Vector2.zero;
    public float angle = Mathf.PI/2;

    private float distance = 10;
    private float height = 7;
    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(Mathf.Cos(angle) * distance, distance * 0.8f, Mathf.Sin(angle) * distance);
    }

    // Update is called once per frame
    void Update()
    {
        PlayerInput();
        transform.position = focusObject.transform.position + offset;
        transform.rotation = Quaternion.LookRotation((focusObject.transform.position + new Vector3(0, 0f, 0)) - transform.position);
    }

    private void PlayerInput()
    {
        if (Input.GetMouseButton(1))
        {
            angle = (angle -Input.GetAxis("Mouse X") * lookSpeed * Time.deltaTime) % Mathf.Rad2Deg;
            height += Input.GetAxis("Mouse Y") * moveSpeed * Time.deltaTime;
            if (height < minHeight)
                height = minHeight;
            if (height > maxHeight)
                height = maxHeight;
            UpdatePos();

        }
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            distance += Input.GetAxis("Mouse ScrollWheel") * 5;
            if (distance < minDistance)
                distance = minDistance;
            if (distance > maxDistance)
                distance = maxDistance;
            //Debug.Log(distance);
            UpdatePos();
        }
    }

    private void UpdatePos()
    {
        offset = new Vector3(Mathf.Cos(angle) * distance, height, Mathf.Sin(angle) * distance);
    }
}
