﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayingPartConnection : MonoBehaviour
{
    public string listeningPart;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == listeningPart)
        {
            other.transform.parent.gameObject.GetComponent<PlayerControl>().DockBody();
            Destroy(transform.parent.gameObject);
        }
    }
}
