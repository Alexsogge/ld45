﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour
{
    public float rotateSpeed = 10;

    private GameObject rodModel;
    private GameObject button;
    private bool isOpen = false;
    private bool opening = false;
    private bool closig = false;

    private float highlightedtime = 0;
    // Start is called before the first frame update
    void Start()
    {
        rodModel = transform.Find("BarrierRod").gameObject;
        button = transform.Find("Button").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (highlightedtime > 0)
        {
            highlightedtime -= Time.deltaTime;
            if (highlightedtime <= 0)
                button.SetActive(false);
        }
        if (opening)
        {
            if (rodModel.transform.rotation.eulerAngles.z >= 180)
            {
                opening = false;
                //rodModel.transform.rotation = Quaternion.Euler(90, 0, 0);
                //rodModel.transform.rotation = Quaternion.LookRotation(Vector3.up);
            } else
            {
                rodModel.transform.Rotate(Vector3.right * Time.deltaTime * rotateSpeed);
            }
        }
        if (closig)
        {
            if (rodModel.transform.rotation.eulerAngles.x >= 180)
            {
                closig = false;
                //rodModel.transform.rotation = Quaternion.LookRotation(Vector3.zero);
            }
            else
            {
                rodModel.transform.Rotate(Vector3.right * Time.deltaTime * -rotateSpeed);
            }
        }
    }

    public void ToggleState()
    {
        if (!isOpen)
        {
            opening = true;
            closig = false;
        } else
        {
            closig = true;
            opening = false;
        }
        isOpen = !isOpen;
    }

    public void Highlight()
    {
        if (highlightedtime <= 0)
        {
            button.SetActive(true);
        }
        highlightedtime = 0.5f;
    }
}
