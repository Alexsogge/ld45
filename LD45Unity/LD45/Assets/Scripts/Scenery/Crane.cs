﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crane : MonoBehaviour
{
    private GameObject button;
    public GameObject liftObject;

    private float highlightedtime = 0;
    // Start is called before the first frame update
    void Start()
    {
        button = transform.Find("Button").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (highlightedtime > 0)
        {
            highlightedtime -= Time.deltaTime;
            if (highlightedtime <= 0)
                button.SetActive(false);
        }
    }

    public void ToggleState()
    {
        Debug.Log("Toggle Crane");
        liftObject.GetComponent<Animator>().SetTrigger("Start");
    }

    public void Highlight()
    {
        if (highlightedtime <= 0)
        {
            button.SetActive(true);
        }
        highlightedtime = 0.5f;
    }
}
